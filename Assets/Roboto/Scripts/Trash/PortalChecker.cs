﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PortalChecker : MonoBehaviour
{
    public static Action<bool> OnTrashInPortal;
    public TrashType trashType;
    private TrashBase trashBehaviour;
    

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Trash")
        {
            trashBehaviour = collision.GetComponent<TrashBase>();
            Debug.Log(trashBehaviour.trashType + " trash type");
            if (ValidateTrash(trashBehaviour))
            {
                if (trashBehaviour.IsTutorialTrash)
                {
                    // search for trash tutorial current state tut
                    TrashTutorialBehaviour trashTutorial = trashBehaviour.GetComponent<TrashTutorialBehaviour>();
                    if (trashTutorial.tutorialState == TutorialState.Throw)
                    {
                        trashTutorial.tutorialState = TutorialState.Helper;
                        TutorialBase.TutorialComplete(TutorialState.Throw);
                        return;
                    }
                    if (trashTutorial.tutorialState == TutorialState.Helper)
                    {
                        //trashTutorial.tutorialState = TutorialState.Enemy;
                        TutorialBase.TutorialComplete(TutorialState.Helper);
                    }
                    
                }
                OnTrashInPortal(true);
            }
            else
            {
                OnTrashInPortal(false);
            }
        }
        
        
    }

    private bool ValidateTrash(TrashBase trash)
    {
        
        if (trash.trashType == trashType)
        {
            
            trash.gameObject.SetActive(false);
            trash.IsInHand = false;
            trash.TransferTrashToPool();
            return true;     
        }
        return false; 
    }
}
