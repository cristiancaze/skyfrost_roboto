﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashPoolManager : MonoBehaviour
{
    private TrashBehaviour[] trashObjects;
    private List<TrashBehaviour> notActiveTrash;
    [SerializeField] private TrashTutorialBehaviour trashTutorialBehaviour; //quitar cuando se implemente vrtk

    public void Initialize()
    {
        trashObjects = GetComponentsInChildren<TrashBehaviour>();
        
        for (int i = 0; i < trashObjects.Length; i++)
        {
            trashObjects[i].Initialize();
            trashObjects[i].Id = i;
        }
        notActiveTrash = new List<TrashBehaviour>();
    }

    public void TrashControl(Transform parent)
    {
        for (int i = 0; i < trashObjects.Length; i++)
        {
            if (trashObjects[i].IsActive == false)
            {
                notActiveTrash.Add(trashObjects[i]);
            }
        }
        int trash = Random.Range(0, notActiveTrash.Count);     
        notActiveTrash[trash].TransferTrash(parent);
        notActiveTrash.Clear();  
    }

    public void TrashControlTutorial(Transform parent)
    {
        trashTutorialBehaviour.TransferTrash(parent);
    }

}
