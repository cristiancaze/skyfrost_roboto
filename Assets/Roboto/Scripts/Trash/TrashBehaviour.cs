﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrashFloorSensor))]
public class TrashBehaviour : TrashBase
{
    public override void Initialize()
    {
        base.Initialize(); 
    }


    public override void TransferTrash(Transform parent)
    {
        base.TransferTrash(parent);
    }

    public override void TransferTrashToPool()
    {
        base.TransferTrashToPool();
    }
}
