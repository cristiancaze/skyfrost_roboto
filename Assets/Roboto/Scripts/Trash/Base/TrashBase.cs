﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrashBase : MonoBehaviour
{
    public TrashType trashType;
    protected Vector3 trashTutorialInitPosition;
    protected Transform defaultParent;
    protected int throwSpeed = 25;
    #region Rig
        protected Rigidbody rig;
        public Rigidbody Rig
        {
            get {return rig;}
            set {rig = value;}
        }
    #endregion
    #region IsTutorialTrash
        private bool isTutorialTrash;
        public bool IsTutorialTrash
        {
            get {return isTutorialTrash;}
            set {isTutorialTrash = value;}
        }
    #endregion
    #region IsGround
        private bool isGround;
        public bool IsGround
        {
            get {return isGround;}
            set {isGround = value;}
        }
    #endregion
    #region Id
        private int id;
        public int Id
        {
            get {return id;}
            set {id = value;}
        }
    #endregion
    #region IsActive
        private bool isActive; 
        public bool IsActive
        {
            get {return isActive;}
            set {isActive = value;}
        }
    #endregion
    #region IsInHand
        private bool isInHand;
        public bool IsInHand
        {
            get {return isInHand;}
            set {isInHand = value;}
        }
    #endregion
    

    private void Awake()
    {
        rig = GetComponent<Rigidbody>();
    }
    
    
    public virtual void Initialize()
    {
        defaultParent = transform.parent;
        gameObject.SetActive(false);  
    }

    public virtual void TransferTrash(Transform parent)
    {
        gameObject.SetActive(true);
        isActive = true;
        transform.SetParent(parent);
        transform.rotation = parent.rotation;
        rig.isKinematic = true;

        transform.position = parent.position + -parent.up * 0.8f; 
    }

    public virtual void TransferTrashToPool()
    {
        isActive = false;
        transform.SetParent(defaultParent);
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
        gameObject.SetActive(false);
    }

    public virtual void ResetPosition()
    {
        isGround = false;
        rig.isKinematic = true;
        transform.SetParent(defaultParent); 
        transform.position = trashTutorialInitPosition;
        transform.rotation = Quaternion.identity;
    }

    public virtual void ResetParent()
    {
        transform.SetParent(null);
    }
}
