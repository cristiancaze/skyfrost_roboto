﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrashTutorialFloorSensor))]
public class TrashTutorialBehaviour : TrashBase
{
    public TutorialState tutorialState;

    public override void Initialize()
    {
        trashTutorialInitPosition = transform.position;
    }

    public override void TransferTrash(Transform parent)
    {
        base.TransferTrash(parent);
    }


    public override void ResetPosition()
    {
        base.ResetPosition();
    }

    public override void TransferTrashToPool()
    {
        base.TransferTrashToPool();
    }

}
