﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashFloorSensor : MonoBehaviour
{
    private TrashBehaviour trashBehaviour;
    private float timeToDissapear = 4;
    
    private void Awake()
    {
        trashBehaviour = transform.GetComponent<TrashBehaviour>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            StartCoroutine(WaitToDissapear());
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            StopCoroutine(WaitToDissapear());
        }
    }

    private IEnumerator WaitToDissapear()
    {
        yield return new WaitForSeconds(timeToDissapear);
        trashBehaviour.IsInHand = false;
        trashBehaviour.TransferTrashToPool();
    }
}
