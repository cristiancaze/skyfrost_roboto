﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashSpotSensor : AIBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "MobHelper")
        {
            MobHelperController.SpotBehaviour(SpotType.OnTrashSpot, collision.gameObject.GetComponent<MobHelperBehaviour>());  
        }
    }
}
