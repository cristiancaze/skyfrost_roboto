﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashSpotType : MonoBehaviour
{
    [SerializeField] private MobHelperType mobReference;
    public MobHelperType MobReference
    {
        get {return mobReference;}
    }
}
