﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashTutorialFloorSensor : MonoBehaviour
{
    private TrashTutorialBehaviour trashTutorialBehaviour;
    private float timeToDissapear = 2;

    private void Awake()
    {
        trashTutorialBehaviour = transform.GetComponent<TrashTutorialBehaviour>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            StartCoroutine(WaitToDissapear());
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            StopCoroutine(WaitToDissapear());
        }
    }

    private IEnumerator WaitToDissapear() {
        yield return new WaitForSeconds(timeToDissapear);
        trashTutorialBehaviour.IsInHand = false;
        trashTutorialBehaviour.ResetPosition();
    }
}
