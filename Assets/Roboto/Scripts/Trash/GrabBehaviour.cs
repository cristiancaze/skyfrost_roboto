﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class GrabBehaviour : VRTK_InteractableObject
{
    private TrashBase trash;
    private GameObject trashTypeCanvas;
    
    
    private void Awake()
    {
        trashTypeCanvas = GetComponentInChildren<Canvas>().gameObject;
        trash = GetComponent<TrashBase>();
        trashTypeCanvas.SetActive(false);
    }

    public override void OnInteractableObjectGrabbed(InteractableObjectEventArgs e)
    {
        base.OnInteractableObjectGrabbed(e);
        if (MobHelperController.TrashPick != null)
        {
            if (!trash.IsInHand)
                MobHelperController.TrashPick(transform.parent.GetComponent<MobHelperBehaviour>());
        }
        trashTypeCanvas.SetActive(true);
        WhileIsUsed();
    }

    public override void OnInteractableObjectUngrabbed(InteractableObjectEventArgs e)
    {
        base.OnInteractableObjectUngrabbed(e);
        // Rigidbody rb = GetComponent<Rigidbody>();
        // rb.AddForce(Vector3.forward * 15, ForceMode.Impulse);
        trashTypeCanvas.SetActive(false);
        WhileIsUsed(); 
    }

    private void WhileIsUsed()
    {
        trash.ResetParent();
        trash.IsInHand = true;
        trash.Rig.isKinematic = false;
    }
}
