﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehaviour : MonoBehaviour
{ 
    protected float speed; 
    protected Transform target;
    protected Transform trashSpot;
    protected NavMeshAgent navMeshAgent;
    protected bool isTutorialTrash;

    public virtual void Initialize()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = speed;
    }
    
    public virtual void GoToPlayer()
    {
        navMeshAgent.SetDestination(target.position);
    }

    public virtual void GoToTrash(bool isTutorialTrash = false)
    {
        navMeshAgent.SetDestination(trashSpot.position);
    }


}
