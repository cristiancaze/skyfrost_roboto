﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobEnemy : AIBehaviour
{
    private Transform defaultParent;

    public void Initialize(Transform player, float  _speed) {
        defaultParent = transform.parent;
        speed = _speed;
        target = player;
        base.Initialize();
    }

    public void SetToSpawnSpot(Transform spawnSpot) {
        transform.SetParent(spawnSpot);
        transform.position = spawnSpot.position;
        transform.rotation = spawnSpot.rotation;
    }

    public override void GoToPlayer()
    {
        base.GoToPlayer();
    }

}
