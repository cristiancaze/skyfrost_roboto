﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobEnemyPoolManager : MonoBehaviour
{
    private MobEnemy[] mobEnemyObjects;
    [SerializeField] private Transform[] spawnSpots;
    private float minSpeed = 0.15f;
    private float maxSpeed = 0.25f;

    public void Initialize(Transform player) {
        mobEnemyObjects = GetComponentsInChildren<MobEnemy>();
        for (int i = 0; i < mobEnemyObjects.Length; i++)
        {
            float _speed = Random.Range(minSpeed, maxSpeed);
            int spawnSpot = Random.Range(0, spawnSpots.Length);
            mobEnemyObjects[i].Initialize(player, _speed);
            mobEnemyObjects[i].SetToSpawnSpot(spawnSpots[spawnSpot]);
            mobEnemyObjects[i].gameObject.SetActive(false);
        }
    }

    public void SendWave() {
        for (int i = 0; i < mobEnemyObjects.Length; i++)
        {
            mobEnemyObjects[i].gameObject.SetActive(true);
            mobEnemyObjects[i].GoToPlayer();
        }
    }
}
