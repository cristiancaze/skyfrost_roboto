﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MobHelperController : MonoBehaviour
{
    private MobHelperBehaviour[] mobs;
    private TrashSpotType[] trashSpots;
    private Transform player;
    private TrashPoolManager trashPool;
    public static Action<MobHelperBehaviour> TrashPick;
    public static Action<SpotType, MobHelperBehaviour> SpotBehaviour;
    public static Action<bool> ActivateMobHelpers;

    private void Awake()
    {
        TrashPick += OnTrashPick;
        SpotBehaviour += OnSpotBehaviour;
        ActivateMobHelpers += OnActivateMobHelpers;
    }

    private void OnDisable()
    {
        TrashPick -= OnTrashPick;
        SpotBehaviour -= OnSpotBehaviour;
        ActivateMobHelpers -= OnActivateMobHelpers;
    }

    public void Initialize(Transform player, TrashSpotType[] trashSpots, TrashPoolManager trashPool, MobHelperBehaviour[] mobs)
    {
        this.mobs = mobs;
        this.player = player;
        this.trashSpots = trashSpots;
        this.trashPool = trashPool;
        InitializeAllMobs();
        HideComboTypes();
    }

    public void SendMobToTrash(MobHelperType mobHelper)
    {
        for (int i = 0; i < mobs.Length; i++)
        {
            if (mobs[i].Type == mobHelper)
            {
                mobs[i].gameObject.SetActive(true);
                mobs[i].GoToTrash();
            }
        }
    }

    private void InitializeAllMobs()
    {
        for (int i = 0; i < mobs.Length; i++)
        {
            if (mobs[i].Type == MobHelperType.Normal)
            {
                mobs[i].Initialize(player, SearchTrashSpot(MobHelperType.Normal).transform); 
            }
            if (mobs[i].Type == MobHelperType.ComboFury)
            {
                mobs[i].Initialize(player, SearchTrashSpot(MobHelperType.ComboFury).transform);
            }
            if (mobs[i].Type == MobHelperType.ComboFury1)
            {
                mobs[i].Initialize(player, SearchTrashSpot(MobHelperType.ComboFury1).transform);
            }
            
        }
    }

    private void StopComboMobs()
    {
        for (int i = 0; i < mobs.Length; i++)
        {
            if (mobs[i].Type == MobHelperType.ComboFury && mobs[i].gameObject.activeSelf || mobs[i].Type == MobHelperType.ComboFury1 && mobs[i].gameObject.activeSelf)
            {
                mobs[i].TransferActiveTrashToPool();
                mobs[i].gameObject.SetActive(false);
            }
        }
    }

    private TrashSpotType SearchTrashSpot(MobHelperType mobType)
    {
        for (int i = 0; i < trashSpots.Length; i++)
        {
            if (trashSpots[i].MobReference == mobType)
            {
                return trashSpots[i];
            }
        }
        return null;
    }


    private void HideComboTypes()
    {
        for (int i = 0; i < mobs.Length; i++)
        {
            if (mobs[i].Type == MobHelperType.ComboFury || mobs[i].Type == MobHelperType.ComboFury1)
            {
                mobs[i].gameObject.SetActive(false);
            }
        }
    }

    private void OnSpotBehaviour(SpotType spot, MobHelperBehaviour mobHelper) {
        switch (spot)
        {
            case SpotType.OnPlayerSpot:
                SendMobToTrash(mobHelper.Type);
            break;

            case SpotType.OnTrashSpot:
                if (GameManager.CurrentGameState == GameStates.Gameplay)
                {
                    trashPool.TrashControl(mobHelper.transform);
                }
                else if (GameManager.CurrentGameState == GameStates.Tutorial)
                {
                    trashPool.TrashControlTutorial(mobHelper.transform);
                }
                
                mobHelper.GoToPlayer();
            break;

            default:
            break;
        }
    }

    private void OnActivateMobHelpers(bool activate)
    {
        if (activate)
        {
            SendMobToTrash(MobHelperType.ComboFury);
            SendMobToTrash(MobHelperType.ComboFury1);
        }
        else 
        {
            StopComboMobs();
        }
        
    }

    private void OnTrashPick(MobHelperBehaviour mobHelper)
    {
        if (GameManager.CurrentGameState == GameStates.Gameplay)
        {
            SendMobToTrash(mobHelper.Type);
        }
    }
    
}
