﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobHelperBehaviour : AIBehaviour
{
    [SerializeField] private MobHelperType type;
    public MobHelperType Type
    {
        get {return type;}
    }
    
    public void Initialize(Transform player, Transform trashSpot)
    {
        this.trashSpot = trashSpot;
        target = player;
        speed = 7;
        base.Initialize(); 
    }

    public override void GoToPlayer()
    {
        base.GoToPlayer();
    }

    public void TransferActiveTrashToPool()
    {
        if (type == MobHelperType.ComboFury || type == MobHelperType.ComboFury1)
        {
            TrashBase trash = GetComponentInChildren<TrashBase>();
            trash.TransferTrashToPool();
        }
    }

    public override void GoToTrash(bool isTutorialTrash = false)
    {
        this.isTutorialTrash = isTutorialTrash;
        base.GoToTrash();
    }
}
