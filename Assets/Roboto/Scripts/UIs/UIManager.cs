﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static Action<UIView, bool> ChangeView;

    private UIBase[] primaryViews;
    private Camera uiCamera;
    private UIBase actualUI;
    [SerializeField] private HUDBehaviour hudBehaviour;

    public void Initialize()
    {
        uiCamera = GetComponentInChildren<Camera>();
        primaryViews = GetComponentsInChildren<UIBase>();
        DefaultViewsValues();
    }

    private void Awake()
    {
        ChangeView += OnChangeView;
    }

    private void OnDisable()
    {
      ChangeView -= OnChangeView;  
    }

    private void DefaultViewsValues()
    {
        for (int i = 0; i < primaryViews.Length; i++)
        {
            primaryViews[i].CloseView(true);
        }  
    }

    private void OnChangeView(UIView view, bool isPrimary)
    {
        if(isPrimary)
        {
            actualUI = SearchView(view);
            actualUI.gameObject.SetActive(true);
            SelectPrimaryView(actualUI, view);
        }
        else
        {
            FactoryView.CreateView(view, transform, uiCamera);
        }
        
    }

    private void SelectPrimaryView(UIBase actualView, UIView selectedView)
    {
        switch (selectedView)
        {
            case UIView.Main:
                actualView.GetComponent<MainBehaviour>().Initialize(uiCamera);
                break;
            case UIView.HUD:
                actualView.GetComponent<HUDBehaviour>().Initialize(uiCamera);
                break;
            default:
                break;
        }
    }
    private UIBase SearchView(UIView _view)
    {
        UIBase view = null;
        for (int i = 0; i < primaryViews.Length; i++)
        {
            if(_view == primaryViews[i].View)
            {
                view = primaryViews[i];
                break;
            }
        }
        
        return view;
    }

    public void UpdateHudData(HUDData data, int combo = 0, float points = 0, int time = 0)
    {
        hudBehaviour.UpdateHud(data, combo, points, time);
    }

}
