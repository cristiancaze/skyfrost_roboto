﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

[RequireComponent(typeof(TimerArcade))]
public class HUDBehaviour : UIBase
{
    private TimerArcade timerArcade;
    [SerializeField] private TextMeshPro time_Txt;
    [SerializeField] private TextMeshPro numberOfCombos_Txt;
    [SerializeField] private TextMeshPro score_Txt;
    

    protected override void Awake()
    {
        base.Awake();
        view = UIView.HUD;
    }

    
    public override void Initialize(Camera camera)
    {
        base.Initialize(camera);
        timerArcade = GetComponent<TimerArcade>();
        timerArcade.Initialise(time_Txt);  
        timerArcade.StartTimer();    
    }

    public void UpdateHud(HUDData data, int combo = 0, float points = 0, int time = 0, bool reward = false)
    {
        switch (data)
        {
            case HUDData.Points:
                score_Txt.text = "Puntos: " + points.ToString();
            break;

            case HUDData.Combo:
                numberOfCombos_Txt.text = "Combos: " + combo.ToString();
            break;

            case HUDData.Time:
                timerArcade.UpdateTimer(time);
            break;

            default:
            break;
        }
    }


}
