﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainBehaviour : UIBase
{
    protected override void Awake()
    {
        base.Awake();
        view = UIView.Main;
    }
    
    public override void Initialize(Camera camera)
    {
        base.Initialize(camera);
    }

    public void GoToGameplay()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
