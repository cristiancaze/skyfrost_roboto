public enum TrashType {
    Ordinary = 0,
    Plastic = 1,
    Paper = 2,
    Tutorial = 3,
    NoType = 4
}

public enum HandType {
    Right = 0,
    Left = 1
}

public enum SpotType {
    OnPlayerSpot = 0,
    OnTrashSpot = 1
}

public enum Character {
    Player = 0,
    MobHelper = 1
}

public enum GameStates {
    Gameplay = 0,
    Pause = 1,
    Tutorial = 2,
    GameOverDefeat = 3,
    GameOverVictory = 4,
    GameOver = 5,

}

public enum TutorialState {
    Throw = 0,
    Helper = 1,
    Enemy = 2
}

public enum UIView
{
    None = -1,
    Main = 0,
    HUD = 1,
    Settings = 2,
    Loading = 3,
    TutorialThrow = 4,
    TutorialMobHelper = 5,
    TutorialEnemys = 6
}

public enum HUDData
{
    Combo = 0,
    Points = 1,
    Time = 2
}

public enum MobHelperType
{
    Normal = 0,
    ComboFury = 1,
    ComboFury1 = 2
}

public enum Scenes
{
    GameOverDefeat = 0,
    GameOverVictory = 1,
    Gameplay = 2,
    Main = 4
}

public enum SFX
{
    Achievement_02 = 0,
    Wrong = 1,
    Explosion = 2
}

