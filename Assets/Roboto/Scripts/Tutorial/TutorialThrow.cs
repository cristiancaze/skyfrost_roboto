﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialThrow : TutorialBase
{
    [SerializeField] private TrashTutorialBehaviour trashTutorialBehaviour;

    public override void Initialize()
    {
        trashTutorialBehaviour.gameObject.SetActive(true);
        trashTutorialBehaviour.Initialize();
        trashTutorialBehaviour.IsTutorialTrash = true;
    }
}
