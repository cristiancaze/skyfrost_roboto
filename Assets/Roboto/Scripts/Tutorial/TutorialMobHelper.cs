﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMobHelper : TutorialBase
{
    [SerializeField] private MobHelperBehaviour mobHelper;
    [SerializeField] private TrashPoolManager trashPoolManager;

    public override void Initialize()
    {
        mobHelper.gameObject.SetActive(true);
        mobHelper.GoToTrash(true);
    }
}
