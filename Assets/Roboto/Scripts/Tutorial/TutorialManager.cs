﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialManager : MonoBehaviour
{
    public Action TutorialsComplete; 
    private TrashTutorialBehaviour trashTutorialBehaviour;
    private TutorialThrow tutorialThrow;
    private TutorialMobHelper tutorialMobHelper;
    private TutorialEnemys tutorialEnemys;  

    private void Awake()
    {
        tutorialThrow = GetComponentInChildren<TutorialThrow>();
        tutorialMobHelper = GetComponentInChildren<TutorialMobHelper>();
        tutorialEnemys = GetComponentInChildren<TutorialEnemys>();

        TutorialBase.TutorialComplete += OnTutorialComplete;
    }
    private void OnDisable()
    {
        TutorialBase.TutorialComplete -= OnTutorialComplete;
    }

    public void StartTutorial()
    {
        DoTutorial(TutorialState.Throw);
    }

    private void DoTutorial(TutorialState tutorialState)
    {
        switch (tutorialState)
        {
            case TutorialState.Throw:
                tutorialThrow.Initialize();
            break;

            case TutorialState.Helper:
                tutorialMobHelper.Initialize();
            break;

            case TutorialState.Enemy:
                tutorialEnemys.Initialize();
            break;

            default:
            break;
        }
    }

    private void OnTutorialComplete(TutorialState tutorialState)
    {
        switch (tutorialState)
        {
            case TutorialState.Throw:
                DoTutorial(TutorialState.Helper);
            break;

            case TutorialState.Helper:
                DoTutorial(TutorialState.Enemy);
            break;

            case TutorialState.Enemy:
                TutorialsComplete();
            break;

            default:
            break;
        }
    }
}
