﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnemys : TutorialBase
{
    [SerializeField] private MobEnemyPoolManager mobEnemyPoolManager;
    [SerializeField] private MobHelperBehaviour mobHelper;

    public override void Initialize()
    {
        mobEnemyPoolManager.SendWave();
        mobHelper.GoToTrash();
    }
}
