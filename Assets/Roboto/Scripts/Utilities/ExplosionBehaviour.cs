﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ExplosionBehaviour : MonoBehaviour
{
    private float radius = 30f;
    private float explosionForce = 300f;
    
    public void DoExplosion() {
       Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

       for (int i = 0; i < colliders.Length; i++)
       {
           Rigidbody enemyRigidBody = colliders[i].GetComponent<Rigidbody>();

           if (enemyRigidBody != null && enemyRigidBody.transform.tag == "MobEnemy") {
               enemyRigidBody.isKinematic = false;
               enemyRigidBody.AddForce(-enemyRigidBody.transform.forward * 6f, ForceMode.Impulse);
               StartCoroutine(WaitForEnableKinematic(enemyRigidBody));
           }
       }
    }

    private IEnumerator WaitForEnableKinematic(Rigidbody rigidBody) {
        yield return new WaitForSeconds(1);
        rigidBody.isKinematic = true;
    }
}
