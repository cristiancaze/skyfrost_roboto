﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    [SerializeField] private ExplosionBehaviour explosionBehaviour;
    [SerializeField] private TimerArcade timerArcade;
    private ComboBehaviour comboBehaviour;
    private UIManager uiManager;
    private int trashInARow = 0;
    private int pointsMultiplier = 1;
    private int points = 0;

    private int trashCombo = 0;

    private void Awake()
    {
        comboBehaviour = GetComponent<ComboBehaviour>();
        PortalChecker.OnTrashInPortal = OnTrashInPortal;
        if (PlayerPrefs.HasKey("Points"))
        {
            PlayerPrefs.SetInt("Points", 0);
        }      
    }

    public void Initialize(UIManager uiManager)
    {
        this.uiManager = uiManager;
    }

    public void SavePointsInPlayerPrefs()
    {
        PlayerPrefs.SetInt("Points", points);
        PlayerPrefs.Save();
    }

    public bool ValidateIfWin()
    {
        bool win = points >= 2500 ?  win = true :  win = false;
        return win;
    }

    public void UpdateCombo(bool correctTrash)
    {
        if (correctTrash)
        {
            trashInARow += 1;
            trashCombo += 1;
            comboBehaviour.ChangeSliderValue(1);
            pointsMultiplier += 200;
            points = points + pointsMultiplier;
        }
        else
        {
            comboBehaviour.ChangeSliderValue(0);
            trashInARow = 0;
            trashCombo = 0;
            pointsMultiplier = 1;
        }
    }


    public bool CheckIfCanExplode()
    {
        if (trashInARow == 2)
        {
            trashInARow = 0;
            return true;
        }
        return false;
    }

    public void PenaliseTimer(int time)
    {
        timerArcade.UpdateTimer(time);
    } 

    private void OnTrashInPortal(bool correctTrash) {
        if (correctTrash)
        {
            // reward
            UpdateCombo(true);
            SoundManager.PlaySound(SFX.Achievement_02.ToString(), false);
            if (CheckIfCanExplode())
            {
                SoundManager.PlaySound(SFX.Explosion.ToString(), false);
                if (GameManager.CurrentGameState == GameStates.Tutorial)
                {
                    TutorialBase.TutorialComplete(TutorialState.Enemy);
                }  
                explosionBehaviour.DoExplosion();
            }
            uiManager.UpdateHudData(HUDData.Time, 0, 0, 3);
            uiManager.UpdateHudData(HUDData.Combo, trashCombo);
            uiManager.UpdateHudData(HUDData.Points, 0, points);
        }
        else
        {
            // penalise
            UpdateCombo(false);
            SoundManager.PlaySound(SFX.Wrong.ToString(), false);
            uiManager.UpdateHudData(HUDData.Combo, trashCombo);
            uiManager.UpdateHudData(HUDData.Time, 0, 0, -3);
        }
    }
}
