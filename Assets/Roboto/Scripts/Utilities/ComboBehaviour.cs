﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboBehaviour : MonoBehaviour
{
    [SerializeField] private Slider comboSlider;
    private float currentSliderValue;


    public void ChangeSliderValue(float value)
    {
        if (value == 0)
        {
            currentSliderValue = value;
            MobHelperController.ActivateMobHelpers(false);
        }
        if (currentSliderValue <= 5)
        {
            currentSliderValue += value;
        }
        if (currentSliderValue == 5)
        {
            MobHelperController.ActivateMobHelpers(true);
        } 
        UpdateComboSlider(); 
    }

    private void UpdateComboSlider()
    {
        comboSlider.value = currentSliderValue * 0.2f;
    }

}
