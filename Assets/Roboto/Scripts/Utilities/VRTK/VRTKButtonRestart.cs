﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Examples;
using VRTK.Controllables;
using UnityEngine.SceneManagement;

public class VRTKButtonRestart : ControllableReactor
{
    protected override void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        base.MaxLimitReached(sender, e);
        SceneManager.LoadScene(Scenes.Gameplay.ToString());
    }
}
