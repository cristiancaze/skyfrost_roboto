﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoNotEnterSpotSensor : MonoBehaviour
{
    public static Action OnMobEnemyInPlayerArea;
    private void OnTriggerEnter(Collider collider) {
        if (collider.transform.tag == "MobEnemy") {
            OnMobEnemyInPlayerArea();
        }
    }
}
