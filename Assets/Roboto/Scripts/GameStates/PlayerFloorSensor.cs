﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayerFloorSensor : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "MobHelper")
        {
            VideoPlayer videoPlayer = collider.GetComponentInChildren<VideoPlayer>();
            videoPlayer.Play();
        }
    }
}
