﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverBehaviour : MonoBehaviour
{
    [SerializeField] private MobHelperBehaviour mobHelper;
    [SerializeField] private GameObject player;
    [SerializeField] private TextMeshProUGUI finalScoreTxt;
    private int points;

    private void Awake()
    {
        mobHelper.Initialize(player.transform, player.transform);
        DoGameOverBehaviour();
        if (PlayerPrefs.HasKey("Points"))
        {
            points = PlayerPrefs.GetInt("Points");
            finalScoreTxt.text = points.ToString();
            Debug.Log("points in over  " +points);
        }
    }

    private void DoGameOverBehaviour()
    {
        mobHelper.GoToPlayer();
    }

}
