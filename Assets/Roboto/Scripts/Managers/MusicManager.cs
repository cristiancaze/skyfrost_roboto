using UnityEngine;
using System.Collections;

/// <summary>
/// Singlento Administrador de la Musica
/// </summary>
/// 
public class MusicManager : MonoBehaviour 
{
	private  AudioSource[] sources;
	private  string actualMusicName;
	private  bool isActualMusicInLoop;

	private static MusicManager _instance;

	private void Awake() 
	{

		sources = _instance.GetComponentsInChildren<AudioSource> ();
	}

	public static void LoadState()
	{
		if (PlayerPrefs.HasKey ("IsMusicActive")) {
			int isAudioActive = PlayerPrefs.GetInt("IsMusicActive");
			if(isAudioActive == 1)
			{
				GlobalObjects.IsMusicActive = true;
			}
			else
			{
				GlobalObjects.IsMusicActive = false;
			}
		} else {
			if(GlobalObjects.IsMusicActive)
			{
				PlayerPrefs.SetInt("IsMusicActive",1);
				
			}else
			{
				PlayerPrefs.SetInt("IsMusicActive",0);
			}
			PlayerPrefs.Save();
		}
	}

	public void PlayMusicByName(string name, bool isLoop)
	{
		actualMusicName = name;
		isActualMusicInLoop = isLoop;
		if (GlobalObjects.IsMusicActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					sources [i].loop = isLoop;
					sources [i].Play ();
				}else
				{
					//sources [i].Stop ();
				}
			}
		}
	}

	public void ActiveMusicByName(string name, bool isLoop, float volume)
	{
		actualMusicName = name;
		isActualMusicInLoop = isLoop;

		if (GlobalObjects.IsMusicActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					if (!sources [i].isPlaying)
					{
						sources [i].loop = isLoop;
						sources [i].Play ();
					}
					sources [i].volume = volume;
				}else
				{
					sources [i].Stop ();
				}
			}
		}
	}

	public void SetMusicVolume(string name, float volume)
	{
		actualMusicName = name;

		if (GlobalObjects.IsMusicActive)
		{
			for (int i = 0; i < sources.Length; i++)
			{
				if (sources[i].gameObject.name == name)
				{
					sources[i].volume = volume;
				}
			}
		}
	}

    public void StopMusicByName(string name, bool isLoop)
    {
        actualMusicName = name;
        isActualMusicInLoop = isLoop;
        if (GlobalObjects.IsMusicActive)
        {
            for (int i = 0; i < sources.Length; i++)
            {
                if (sources[i].gameObject.name == name)
                {
                    sources[i].loop = isLoop;
                    sources[i].Stop();
                }
            }
        }
    }

    public void ActiveAll()
	{
		GlobalObjects.IsMusicActive = true;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = true;
		}
		PlayerPrefs.SetInt("IsMusicActive",1);
		PlayerPrefs.Save();
		PlayMusicByName (actualMusicName, isActualMusicInLoop);
	}

	public void DesactiveAll()
	{
		GlobalObjects.IsMusicActive = false;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = false;
		}
		PlayerPrefs.SetInt("IsMusicActive",0);
		PlayerPrefs.Save();
	}

	public void SilenceAll()
	{
		if (GlobalObjects.IsMusicActive) {
			for (int i = 0; i < sources.Length; i++) {
				sources [i].enabled = true;
				sources [i].volume = 0;
			}
		}
	}

	public void StopAll()
	{
		if (GlobalObjects.IsMusicActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].isPlaying) {
					sources [i].Stop ();
				}
			}
		}
	}
}
