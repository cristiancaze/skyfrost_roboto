﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private TrashPoolManager trashPoolManager;
    [SerializeField] private MobEnemyPoolManager mobEnemyPoolManager;
    [SerializeField] private MobHelperController mobHelperController;
    [SerializeField] private TutorialManager tutorialManager;
    [SerializeField] private UIManager uiManager;
    [SerializeField] private TrashSpotType[] trashSpots;
    [SerializeField] private MobHelperBehaviour[] mobs;
    [SerializeField] private ScoreSystem scoreSystem;
    public static GameStates CurrentGameState;
    public static Action<GameStates> GameState;

    void Start()
    {
        uiManager.Initialize();
        trashPoolManager.Initialize();
        scoreSystem.Initialize(uiManager);
        mobHelperController.Initialize(player.transform, trashSpots, trashPoolManager, mobs);
        mobEnemyPoolManager.Initialize(player.transform);
        tutorialManager.TutorialsComplete += OnTutorialsComplete;
        DoNotEnterSpotSensor.OnMobEnemyInPlayerArea += OnMobEnemyInPlayerArea;

        GameState += OnGameState;
        OnGameState(GameStates.Gameplay);
    }

    private void OnDisable()
    {
        GameState -= OnGameState;
        tutorialManager.TutorialsComplete -= OnTutorialsComplete;
        DoNotEnterSpotSensor.OnMobEnemyInPlayerArea -= OnMobEnemyInPlayerArea;    
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            //GameState(GameStates.Gameplay);
        }
    }

    private void OnGameState(GameStates gameState)
    {
        switch (gameState)
        {
            case GameStates.Tutorial:
                CurrentGameState = GameStates.Tutorial;
                tutorialManager.StartTutorial();   
            break;

            case GameStates.Gameplay:
                CurrentGameState = GameStates.Gameplay;
                if (UIManager.ChangeView != null)
                    UIManager.ChangeView(UIView.HUD, true);
                mobHelperController.SendMobToTrash(MobHelperType.Normal);
                mobEnemyPoolManager.SendWave();
            break;

            case GameStates.GameOver:
            scoreSystem.SavePointsInPlayerPrefs();
            if (scoreSystem.ValidateIfWin())
            {
                SceneManager.LoadScene(Scenes.GameOverVictory.ToString());  
            } else 
            {
                SceneManager.LoadScene(Scenes.GameOverDefeat.ToString());
            }      
            break;

            default:
            break;
        }
    }

    private void OnTutorialsComplete()
    {
        OnGameState(GameStates.Gameplay);
    }

    

    private void OnMobEnemyInPlayerArea()
    {
        scoreSystem.PenaliseTimer(-5);
    }

    
}
